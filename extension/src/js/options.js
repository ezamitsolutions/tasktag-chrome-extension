$(function () {

  chrome.storage.sync.get(['auth_token'], function (settings) {
    $('#auth_token').val(settings.auth_token);
  });

  $('#save_button').click(function () {
    let auth_token = $("#auth_token").val();
    if (auth_token) {
      chrome.storage.sync.set({
        'auth_token': auth_token
      }, function () {
        displayMessage("Token added.");
      });
    }
  });

  $('#remove_token').click(function () {
    chrome.storage.sync.set({
      'auth_token': ""
    }, function () {
      $('#auth_token').val("");
      displayMessage("Token removed.");
    });
  });
});


function displayMessage(message) {
  $("#message").text(message);
}