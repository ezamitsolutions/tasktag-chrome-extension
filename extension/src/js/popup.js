let auth_token;
let api_url_base = "http://0.0.0.0:8000/api/";

chrome.storage.sync.get(["auth_token"], function(settings) {
  if (settings.auth_token) {
    $(".not-auth").hide();
    $(".auth").show();
  }
  $("#auth_token").val(settings.auth_token);
  auth_token = "Token " + settings.auth_token; //Rename token for Django REST Framework

  // Fill up select list with tasks.
  $(function() {
    var url = api_url_base + "get-tasks";
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
      if (xhr.status == 401) {
        $(".not-auth").show();
        $(".auth").hide();
        $(".invalid-token").text("Invalid token");
        $(".invalid-token").show();
      }
      if (xhr.readyState == XMLHttpRequest.DONE) {
        var json = JSON.parse(xhr.responseText);

        for (var x = 0; x < json.length; x++) {
          document.getElementById("tasks").innerHTML +=
            "<option value=" +
            json[x]["id"] +
            ">" +
            json[x]["name"] +
            "</option>";
        }
      }
    };
    xhr.open("GET", url, true);
    xhr.setRequestHeader("Authorization", auth_token);
    xhr.send();
  });
});

// Get username
$(function() {
  var url = api_url_base + "get-username";
  var xhr = new XMLHttpRequest();

  xhr.onreadystatechange = function() {
    if (xhr.status == 401) {
      $(".not-auth").show();
      $(".auth").hide();
      $(".invalid-token").text("Invalid token");
      $(".invalid-token").show();
    }
    if (xhr.readyState == XMLHttpRequest.DONE) {
      var json = JSON.parse(xhr.response);
      $("#username").text(json["username"]);
    }
  };

  xhr.open("GET", url, true);
  xhr.setRequestHeader("Authorization", auth_token);
  xhr.send();
});

// Once the HTML is loaded:
// add listener to both buttons
// save last selected task so when you open up the extension again,
// you get it back and not the default one.
document.addEventListener("DOMContentLoaded", function() {
  document
    .querySelector("#save_selected_text")
    .addEventListener("click", getSelectionText);
  document
    .querySelector("#save_article")
    .addEventListener("click", save_article);

  document
    .querySelector("#tasks")
    .addEventListener("change", checkSelectedOption, false);
});

String.prototype.replaceAll = function(target, replacement) {
  return this.split(target).join(replacement);
};

function checkSelectedOption() {
  let e = document.getElementById("tasks");
  let value = e.options[e.selectedIndex].value;

  // Save selected task.
  chrome.storage.sync.set({
    selectedIndex: value
  });
}

////////////////////////////////////////////
// Get selected text to save it through AJAX
////////////////////////////////////////////
function getSelectionText() {
  var taskSelectId = document.getElementById("tasks");
  var taskValue = taskSelectId.options[taskSelectId.selectedIndex].value;
  if (taskValue != 0) {
    chrome.tabs.executeScript(
      {
        code:
          "var x = new Array(2);x[0] = document.documentURI;var selection = window.getSelection();var range;range = selection.getRangeAt(0);var clonedSelection = range.cloneContents(); var div = document.createElement('div');\
    div.appendChild(clonedSelection); x[1] = div.innerHTML; x;"
      },
      function(selection) {
        var xhr = new XMLHttpRequest();
        xhr.open("POST", api_url_base + "set-selected-text", true);
        xhr.setRequestHeader(
          "Content-type",
          "application/x-www-form-urlencoded"
        );
        xhr.setRequestHeader("Authorization", auth_token);
        xhr.onloadstart = function(e) {
          loading.style.display = "block";
        };
        xhr.onloadend = function(e) {
          loading.style.display = "none";
        };
        xhr.onreadystatechange = function(aEvt) {
          if (xhr.readyState == 4) {
            if (xhr.status == 200) {
              let json_response = JSON.parse(xhr.response);
              setTimeout(function() {
                document.getElementById("response").innerHTML =
                  json_response["message"];
              }, 100);
            } else
              document.getElementById("response").innerHTML +=
                "Failed!" + xhr.statusText;
          }
        };
        selection[0][1] = selection[0][1].replaceAll(";", "SeMiCoLoN");
        selection[0][1] = selection[0][1].replaceAll("&", "AnD-");
        selection[0][1] = selection[0][1].replaceAll("<br>", "");
        selection[0][1] = selection[0][1].replaceAll("href=", "href1=");

        xhr.send(
          "type=text&content=" +
            selection[0][1] +
            "&url=" +
            selection[0][0] +
            "&task_id=" +
            taskValue
        );
      }
    );
  }
}

////////////////////////////////////////////
// Get article to save it through AJAX
////////////////////////////////////////////
function save_article() {
  var taskSelectId = document.getElementById("tasks");
  var taskValue = taskSelectId.options[taskSelectId.selectedIndex].value;

  if (taskValue != 0) {
    chrome.tabs.executeScript(
      {
        code:
          "var x = new Array(2);x[0] = document.documentURI; x[1] = window.location.hostname; x;"
      },
      function(selection) {
        var xhr = new XMLHttpRequest();
        xhr.open("POST", api_url_base + "set-article", true);
        xhr.setRequestHeader(
          "Content-type",
          "application/x-www-form-urlencoded"
        );
        xhr.setRequestHeader("Authorization", auth_token);
        xhr.onloadstart = function(e) {
          loading.style.display = "block";
        };
        xhr.onloadend = function(e) {
          loading.style.display = "none";
        };
        xhr.onreadystatechange = function(aEvt) {
          if (xhr.readyState == 4) {
            if (xhr.status == 200) {
              let json_response = JSON.parse(xhr.response);
              setTimeout(function() {
                if (xhr.responseText == "Exists") {
                  document.getElementById("response").innerHTML =
                    "Already exists!";
                } else document.getElementById("response").innerHTML = json_response["message"];
              }, 100);
            } else
              document.getElementById("response").innerHTML =
                "Failed! " + xhr.responseText;
          }
        };
        xhr.send(
          "url=" +
            selection[0][0] +
            "&task_id=" +
            taskValue +
            "&urlname=" +
            selection[0][1]
        );
      }
    );
  }
}
